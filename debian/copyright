Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0
Upstream-Name: drawtiming
Upstream-Contact: Edward Counce <ecounce@users.sourceforge.net>
Source: https://drawtiming.sourceforge.net/

Files: *
Copyright: 2004-2007 Edward Counce
License: GPL-2+

Files: src/timing.cc
Copyright: 2004 Edward Counce
           2006-2007 Salvador E. Tropea
           2008 Daniel Beer
License: GPL-2+

Files: src/timing.h
Copyright: 2004 Edward Counce
           2008 Daniel Beer
License: GPL-2+

Files: src/main.cc
Copyright: 2004-2007 Edward Counce
           2006-2007 Salvador E. Tropea
           2008 Daniel Beer
License: GPL-2+

Files: src/parser.*
Copyright: 1984,1989,1990,2000-2006 Free Software Foundation, Inc.
License: GPL-2+

Files: debian/*
Copyright: 2005-2009 Wesley J. Landaker <wjl@icecavern.net>
           2010,2012,2014,2018,2023-2024 أحمد المحمودي (Ahmed El-Mahmoudy) <aelmahmoudy@users.sourceforge.net>
License: GPL-2+

License: GPL-2+
 This program is free software; you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation; either version 2 of the License, or
 (at your option) any later version.
 .
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 .
 You should have received a copy of the GNU General Public License along
 with this program; if not, write to the Free Software Foundation, Inc.,
 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 .
 On Debian systems, the complete text of the GNU GPL2 licenses
 can be found at `/usr/share/common-licenses/GPL-2'.
